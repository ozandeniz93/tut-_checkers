﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class Server : MonoBehaviour
{
    public int port = 6321;


    private List<ServerClient> clients;
    private List<ServerClient> disconnectedList;

    private TcpListener server;
    private bool isServerStarted;

    public void Init()
    {
        // If scene changes, don't destroy the server.
        DontDestroyOnLoad(gameObject);

        clients = new List<ServerClient>();
        disconnectedList = new List<ServerClient>();

        try
        {
            server = new TcpListener(IPAddress.Any, port);
            server.Start();

            StartListening();
            isServerStarted = true;
        }
        catch(Exception e)
        {
            Debug.Log("Server Initialize exception: " + e.Message);
        }
    }

    private void Update()
    {
        if (!isServerStarted)
            return;

        foreach (ServerClient c in clients)
        {
            // Is the client still connected?
            if (!IsConnected(c.tcp))
            {
                // Close the tcp socket
                c.tcp.Close();
                disconnectedList.Add(c);
                continue;
            }
            else
            {
                NetworkStream stream = c.tcp.GetStream();
                if (stream.DataAvailable)
                {
                    StreamReader reader = new StreamReader(stream, true);
                    string data = reader.ReadLine();

                    if(data != null)
                    {
                        OnIncomingData(c, data);
                    }
                }
            }
        }

        for (int i = 0; i < disconnectedList.Count - 1; i++)
        {
            // Tell player somebody has disconnected

            clients.Remove(disconnectedList[i]);
            disconnectedList.RemoveAt(i);
        }
    }

    private void StartListening()
    {
        server.BeginAcceptTcpClient(AcceptTcpClient, server);
    }
    private void AcceptTcpClient(IAsyncResult result)
    {
        TcpListener listener = (TcpListener)result.AsyncState;
 
        ServerClient client = new ServerClient(listener.EndAcceptTcpClient(result));
        clients.Add(client);

        
        StartListening();
        Debug.Log("Somebody has connected.");


    }

    private bool IsConnected(TcpClient client)
    {
        try
        {
            if(client != null && client.Client != null && client.Client.Connected)
            {
                if(client.Client.Poll(0, SelectMode.SelectRead))
                {
                    // ????
                    // Peek the clients and if there is anything to peek return true.
                    return !(client.Client.Receive(new byte[1], SocketFlags.Peek) == 0);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }
    }

    // Server send
    private void Broadcast(string data, List<ServerClient> clientList)
    {
        foreach (var client in clientList)
        {
            try
            {
                StreamWriter writer = new StreamWriter(client.tcp.GetStream());
                writer.WriteLine(data);
                // Clear the buffer.
                writer.Flush();
            }
            // Client is not reachable
            catch (Exception e)
            {
                Debug.Log("Broadcast error: " + e.Message);
            }
        }
    }
    // Listening incoming messages/datas every single frame, server read.
    private void OnIncomingData(ServerClient client, string data)
    {
        Debug.Log(client.clientName + ": " + data);
    }
}

public class ServerClient
{
    public string clientName;
    public TcpClient tcp;

    public ServerClient(TcpClient tcp)
    {
        this.tcp = tcp;
    }
}

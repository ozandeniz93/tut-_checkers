﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject hostMenu;
    public GameObject connectMenu;

    public GameObject serverPrefab;
    public GameObject clientPrefab;

    public InputField nameInput;


    public static GameManager Instance { get; set; }
    private void Start()
    {
        Instance = this;
        hostMenu.SetActive(false);
        connectMenu.SetActive(false);
        DontDestroyOnLoad(gameObject);
    }

    public void ConnectButton()
    {
        mainMenu.SetActive(false);
        connectMenu.SetActive(true);
    }
    public void HostButton()
    {
        try
        {
            Server server = Instantiate(serverPrefab).GetComponent<Server>();
            server.Init();

            // This is the client (user) of the server
            Client client = Instantiate(clientPrefab).GetComponent<Client>();

            client.clientName = nameInput.text;
            if (client.clientName == "")
            {
                client.clientName = "Host";
            }

            client.ConnectToServer("127.0.0.1", 6321);
        }
        catch (System.Exception e)
        {
            Debug.Log("Host error: " + e.Message);
        }


        mainMenu.SetActive(false);
        hostMenu.SetActive(true);
    }

    public void BackButton()
    {
        mainMenu.SetActive(true);

        hostMenu.SetActive(false);
        connectMenu.SetActive(false);

        Server s = FindObjectOfType<Server>();
        if (s != null)
        {
            Destroy(s.gameObject);
        }

        Client c = FindObjectOfType<Client>();
        if (c != null)
        {
            Destroy(c.gameObject);
        }
    }

    public void ConnectToServerButton()
    {
        string hostAddress = GameObject.Find("HostInput").GetComponent<InputField>().text;

        if(hostAddress == "")
        {
            hostAddress = "127.0.0.1";

            try
            {
                Client client = Instantiate(clientPrefab).GetComponent<Client>();
                client.clientName = nameInput.text;
                if(client.clientName == "")
                {
                    client.clientName = "Client";
                }

                client.ConnectToServer(hostAddress, 6321);

                connectMenu.SetActive(false);

                // Change scene
            }
            catch (System.Exception e)
            {
                Debug.Log("Server connection error: " + e.Message);
            }
        }
    }

}

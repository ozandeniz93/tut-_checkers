﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{
    public bool isWhite;
    public bool isKing;

    public bool IsForceToMove(Piece[,] boardPieces, int piecePosX, int piecePosY)
    {
        if (isWhite || isKing)
        {
            // Top-Left
            if(piecePosX >= 2 && piecePosY <= 5)
            {
                Piece p = boardPieces[piecePosX - 1, piecePosY + 1];

                if (p != null && p.isWhite != isWhite)
                {
                    if (boardPieces[piecePosX - 2, piecePosY + 2] == null)
                    {
                        return true;
                    }
                }
            }

            // Top-Right
            if (piecePosX <= 5 && piecePosY <= 5)
            {
                Piece p = boardPieces[piecePosX + 1, piecePosY + 1];

                if (p != null && p.isWhite != isWhite)
                {
                    if (boardPieces[piecePosX + 2, piecePosY + 2] == null)
                    {
                        return true;
                    }
                }
            }
        }

        if(!isWhite || isKing)
        {
            // Bottom-Left
            if (piecePosX >= 2 && piecePosY >= 2)
            {
                Piece p = boardPieces[piecePosX - 1, piecePosY - 1];

                if (p != null && p.isWhite != isWhite)
                {
                    if (boardPieces[piecePosX - 2, piecePosY - 2] == null)
                    {
                        return true;
                    }
                }
            }

            // Bottom-Right
            if (piecePosX <= 5 && piecePosY >= 2)
            {
                Piece p = boardPieces[piecePosX + 1, piecePosY - 1];

                if (p != null && p.isWhite != isWhite)
                {
                    if (boardPieces[piecePosX + 2, piecePosY - 2] == null)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public bool ValidMove(Piece[,] boardPieces, int startX, int startY, int endX, int endY)
    {
        // If the player is moving on top of another piece
        if(boardPieces[endX,endY] != null)
        {
            return false;
        }

        int tileMoveX = Mathf.Abs(startX - endX);
        int tileMoveY = endY - startY;

        // For the white piece
        if(isWhite || isKing)
        {
            if(tileMoveX == 1)
            {
                if (tileMoveY == 1)
                {
                    return true;
                }
            }
            else if(tileMoveX == 2)
            {
                if(tileMoveY == 2)
                {
                    Piece p = boardPieces[(startX + endX) / 2,(startY+endY) / 2];
                    if (p != null && p.isWhite != isWhite)
                    {
                        return true;
                    }
                }
            }
        }

        // For the black piece
        if (!isWhite || isKing)
        {
            if (tileMoveX == 1)
            {
                if (tileMoveY == -1)
                {
                    return true;
                }
            }
            else if (tileMoveX == 2)
            {
                if (tileMoveY == -2)
                {
                    Piece p = boardPieces[(startX + endX) / 2, (startY + endY) / 2];
                    if (p != null && p.isWhite != isWhite)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

}

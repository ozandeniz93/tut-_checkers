﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckersBoard : MonoBehaviour
{
    int pieceLength = 8;

    public Piece[,] pieces = new Piece[8, 8];

    public GameObject whitePiecePrefab;
    public GameObject blackPiecePrefab;

    List<Piece> forcedPieces;

    public bool isPlayerWhite;
    bool isWhiteTurn;

    bool hasKilled;

    Vector3 boardOffset = new Vector3(-4f, .1f, -4f);
    Vector3 pieceOffset = new Vector3(.5f, 0, .5f);

    Vector2 mouseOver;
    Vector2 startDrag;
    Vector2 endDrag;

    Piece selectedPiece;



    private void Start()
    {
        isWhiteTurn = true;
        GenerateBoard();
        forcedPieces = new List<Piece>();
    }
    private void Update()
    {
        UpdateMouseOver();
        Debug.Log(mouseOver);


        if((isPlayerWhite) ? isWhiteTurn : !isWhiteTurn)
        { 
            int x = (int)mouseOver.x;
            int y = (int)mouseOver.y;

            if (selectedPiece != null)
            {
                UpdatePieceDrag(selectedPiece);
            }

            if (Input.GetMouseButtonDown(0))
            {
                SelectPiece(x, y);
            }

            if (Input.GetMouseButtonUp(0))
            {
                TryMove((int)startDrag.x, (int)startDrag.y, x, y);
            }
        }
    }

    private void GenerateBoard()
    {
        // White Team
        for (int y = 0; y < 3; y++)
        {
            bool oddRow = (y % 2 == 0);
            for (int x = 0; x < 8; x += 2)
            {
                // Generate piece
                GeneratePiece(whitePiecePrefab, (oddRow) ? x : x + 1, y);
            }
        }

        // Black Team
        for (int y = 7; y > 4; y--)
        {
            bool oddRow = (y % 2 == 0);
            for (int x = 0; x < 8; x += 2)
            {
                // Generate piece
                GeneratePiece(blackPiecePrefab, (oddRow) ? x : x + 1, y);
            }
        }
    }
    private void GeneratePiece(GameObject prefab, int x, int y)
    {
        GameObject piece = Instantiate(prefab) as GameObject;
        piece.transform.SetParent(gameObject.transform);

        Piece p = piece.GetComponent<Piece>();
        pieces[x, y] = p;

        MovePiece(p, x, y);
    }


    private void UpdateMouseOver()
    {
        if (!Camera.main)
        {
            Debug.Log("There is no main camera!");
        }

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25f, LayerMask.GetMask("Board")))
        {
            mouseOver.x = (int)(hit.point.x - boardOffset.x);
            mouseOver.y = (int)(hit.point.z - boardOffset.z);
        }
        else
        {
            mouseOver.x = -1f;
            mouseOver.y = -1f;
        }
    }
    private void UpdatePieceDrag(Piece p)
    {
        if (!Camera.main)
        {
            Debug.Log("There is no main camera!");
        }

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25f, LayerMask.GetMask("Board")))
        {
            p.transform.position = hit.point + Vector3.up;
        }
       
    }

    private List<Piece> ScanForPossibleMove()
    {
        forcedPieces = new List<Piece>();

        for(int i = 0; i < pieceLength; i++)
        {
            for (int j = 0; j < pieceLength; j++)
            {
                if(pieces[i,j] != null && pieces[i,j].isWhite == isWhiteTurn)
                {
                    if(pieces[i,j].IsForceToMove(pieces, i, j))
                    {
                        forcedPieces.Add(pieces[i, j]);
                    }
                }
            }
        }

        return forcedPieces;
    }
    private List<Piece> ScanForPossibleMove(Piece p, int moveX, int moveY)
    {
        forcedPieces = new List<Piece>();

        if (pieces[moveX, moveY].IsForceToMove(pieces, moveX, moveY))
        {
            forcedPieces.Add(pieces[moveX, moveY]);
        }

        return forcedPieces;
    }

    private void MovePiece(Piece p, int x, int y)
    {
        p.transform.position = (Vector3.right * x) + (Vector3.forward * y) + boardOffset + pieceOffset;
    }
    private void SelectPiece(int x, int y)
    {
        // If we are not on the board
        if(x < 0 || y < 0 || x >= pieceLength || y >= pieceLength)
        {
            return;
        }

        Piece p = pieces[x, y];

        if(p != null && p.isWhite == isPlayerWhite)
        {
            if(forcedPieces.Count == 0)
            {
                selectedPiece = p;
                startDrag = mouseOver;
            }

            if(forcedPieces.Find(fp=>fp==p) == null)
            {
                return;
            }

            selectedPiece = p;
            startDrag = mouseOver;

        }



    }
    private void TryMove(int dragX, int dragY, int mouseX, int mouseY)
    {
        forcedPieces = ScanForPossibleMove();

        // Multiplayer purposes
        startDrag = new Vector2(dragX, dragY);
        endDrag = new Vector2(mouseX, mouseY);

        selectedPiece = pieces[dragX, dragY];

        // If the mouse is out from our board
        if (mouseX < 0 || mouseY < 0 || mouseX >= pieceLength|| mouseY >= pieceLength)
        {
            if(selectedPiece != null)
            {
                MovePiece(selectedPiece, dragX, dragY);
            }

            startDrag = Vector2.zero;
            selectedPiece = null;
            return;
        }

        
        if (selectedPiece != null)
        {
            // If the player wants to not move, if the move is cancelled.
            if(endDrag == startDrag)
            {
                MovePiece(selectedPiece, (int)endDrag.x, (int)endDrag.y);
                startDrag = Vector2.zero;
                selectedPiece = null;
                return;
            }

            // If it is a valid move
            if(selectedPiece.ValidMove(pieces, (int)startDrag.x, (int)startDrag.y, (int)endDrag.x, (int)endDrag.y))
            {
                // Did we kill anything?
                if(Mathf.Abs((int)endDrag.x - (int)startDrag.x) == 2)
                {
                    Piece p = pieces[(dragX + mouseX) / 2, (dragY + mouseY) / 2];
                    if(p != null)
                    {
                        pieces[(dragX + mouseX) / 2, (dragY + mouseY) / 2] = null;
                        Destroy(p.gameObject);
                        hasKilled = true;
                    }
                }

                // If we forced to kill a piece
                if(forcedPieces.Count != 0 && !hasKilled)
                {
                    MovePiece(selectedPiece, (int)startDrag.x, (int)startDrag.y);
                    startDrag = Vector2.zero;
                    selectedPiece = null;
                    return;
                }

                pieces[mouseX, mouseY] = selectedPiece;
                pieces[dragX, dragY] = null;

                MovePiece(selectedPiece, mouseX, mouseY);

                EndTurn();
            }
            else
            {
                // If move is not valid, go back to your place.
                MovePiece(selectedPiece, (int)startDrag.x, (int)startDrag.y);
                startDrag = Vector2.zero;
                selectedPiece = null;
                return;
            }

        }
    }
    private void EndTurn()
    {

        // Becoming a piece king.
        if(selectedPiece != null)
        {
            if(selectedPiece.isWhite && !selectedPiece.isKing && (int)endDrag.y == 7)
            {
                selectedPiece.isKing = true;
                selectedPiece.transform.Rotate(Vector3.right * 180);
            }
            else if (!selectedPiece.isWhite && !selectedPiece.isKing && (int)endDrag.y == 0)
            {
                selectedPiece.isKing = true;
                selectedPiece.transform.Rotate(Vector3.right * 180);
            }
        }



        selectedPiece = null;
        startDrag = Vector2.zero;

        if(ScanForPossibleMove(selectedPiece, (int)endDrag.x, (int)endDrag.y).Count != 0 && hasKilled)
        {
            return;
        }

        isWhiteTurn = !isWhiteTurn;
        hasKilled = false;

        CheckVictory();
    }

    private void CheckVictory()
    {
        var pcs = FindObjectsOfType<Piece>();
        bool hasWhite = false;
        bool hasBlack = false;

        for (int i = 0; i < pcs.Length; i++)
        {
            if (pcs[i].isWhite)
                hasWhite = true;
            else
                hasBlack = true;
        }

        if (hasWhite)
            Victory(true);
        if (hasBlack)
            Victory(false);



    }
    private void Victory(bool isWhite)
    {
        if (isWhite)
            Debug.Log("White win!");
        else
            Debug.Log("Black Win!");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.IO;
using UnityEngine;


public class Client : MonoBehaviour
{
    public string clientName;

    private bool isSocketReady;
    private TcpClient socket;
    private NetworkStream stream;
    private StreamWriter writer;
    private StreamReader reader;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    void Update()
    {
        if (isSocketReady)
        {
            if (stream.DataAvailable)
            {
                string data = reader.ReadLine();
                if(data != null)
                {
                    OnIncomingData(data);
                }
            }
        }
    }





    public bool ConnectToServer(string host, int port)
    {
        if (isSocketReady)
            return false;

        try
        {
            socket = new TcpClient(host, port);
            stream = socket.GetStream();
            writer = new StreamWriter(stream);
            reader = new StreamReader(stream);

            isSocketReady = true;
        }
        catch (System.Exception e)
        {
            Debug.Log("Socket Error, could not connect to server: " + e.Message);
        }

        return isSocketReady;
    }

    // Read message from the server, only communicates with the server.
    private void OnIncomingData(string data)
    {
        Debug.Log(data);
    }
    //Send message to the server
    public void Send(string data)
    {
        if (!isSocketReady)
            return;

        writer.WriteLine(data);
        writer.Flush();
    }

    // In case of crash or disconnection
    private void CloseSocket()
    {
        if (!isSocketReady)
            return;

        writer.Close();
        reader.Close();
        socket.Close();

        isSocketReady = false;
    }

    void OnApplicationQuit()
    {
        CloseSocket();
    }
    void OnDisable()
    {
        CloseSocket();
    }
}

public class GameClient
{
    public string name;
    public bool isHost;
}
